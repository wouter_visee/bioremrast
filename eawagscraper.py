import urllib2 #for opening web pages

import sys
from bs4 import BeautifulSoup as bs #web scraping library
import re #regular expressions
import pandas as pd
import random


def compound_list():
    """
    Returns the compound list as urls, we store the ids here
    """
    compounds = {
        'Tolueen': 'c0114',
        'Anthracene': 'c0991'
    }

    baseurl = 'http://eawag-bbd.ethz.ch/servlets/pageservlet?ptype=c&compID='

    returnlist = [ baseurl + cid for cid in compounds.values() ]
    return returnlist

def reaction_db_from_compoundlists(compoundlist):
    """
    For each compound in the list: get the reactions, store relevant info meaning:
    - ec
    - gene name
    - enc-compound ID
    - reacID
    - URL to ncbi
    At the end, write the CSV to disk
    """
    reactiondbcolumns = ['ec', 'genename', 'compoundid', 'reacid', 'ncbiurl']
    reactiondb = pd.DataFrame(columns=reactiondbcolumns)

    for compound in compoundlist:
        res = reaction_dict_from_url(compound)
        reactiondb = reactiondb.append(res, ignore_index=True)

    print 'reaction db made succesfully!'

def reaction_dict_from_url(url):
    """
    Here we get the actual info from a reaction URL
    """
    reactionsoup = bs(urllib2.urlopen(url), 'lxml')

    myre = re.compile(r'.\.' + #start with anything, then a dot
                      r'\d*\.' + #then an amount of digits, followed by another dot
                      r'\d*\.' +  #more digits, followed by one dot
                      r'.') #ends with everything
    ec = str( reactionsoup.find( string = myre) )

    #now find the gene name: this is harder. First find the compound link, navigate from there to the element
    #containing the name, and then clean up the text
    myre = re.compile( '.compID.' )
    compoundelement = reactionsoup.find( href = myre)
    nameinhere = compoundelement.next.next.next.next.next.next.next.next
    namelist = str(nameinhere).split()
    textregex = re.compile('.[a-z]*.', flags=re.IGNORECASE)
    name = ""
    for el in namelist:
        textfromel = textregex(el)
        if textfromel is not None:
            name.append(textfromel)








def gene_db_from_links(links):
    """
    For each gene in the links: get html, get relevant info:
    - reacID
    - accession number
    - fasta report (don't split this up)
    """



def start_scraping():
    """
    First get the hand-curated compound list
    Use that to make a reaction database, using eawag
    Use the links to make a gene database
    """
    compoundlist = compound_list()
    reactiondb = reaction_db_from_compoundlists(compoundlist)
    links = reactiondb['genebanklinks']
    genedb = gene_db_from_links(links)

if __name__ == 'main':
    start_scraping()