"""
Created on Tue Mar  7 15:19:16 2017

@author: Wouter Visee

Get all genes relevant for hydrocarbon bioremediation from KEGG so we can see how bacteria do it in the Griftpark
Why KEGG and not GO or envigene? because it's easy to program with just KEGG (we only need 1 DB) and its likely
complete enough

Known issues: 
csvs that are open raise exceptions
csvs get overwritten by iterating over a new pathways
database is probably full of duplicate rows
TQDM is a shite so it bugs (set changed during iteration, don't think this has an effect on the program?

TODO:
[] fix tqdm
[] fix duplicate rows
[]fix empty values in dataframes
[x]fix all files in the same folder
[x] fix get genedb from geneids concatenation-error
"""

import urllib2 # for opening web pages
import sys
from bs4 import BeautifulSoup as bs # web scraping library
import pandas as pd
import random
import time # random delay for humanization / load lowering
from tqdm import tqdm # progress bar
import tqdm as tqdmlib
import re # regular expresions
import os
import unicodedata # removing strange spaces from strings
# import progressbar

pathways1 = ['01220', '00624', '00623', '00633']
pathways2 = ['00642', '00622', '00626']
pathways = pathways1 + pathways2
database_headers = ['pathway', 'organism', 'sequence id', 'nucleotide seq', 'aa seq']

"""
debug = True means that every for-loop with the potential of taking a long time
gets broken out of after 10 iterations for a faster runthrough of the whole program

"""
debug = True
humanize_queries = False


def get_soup_from_url_with_robustness(url):
    """
    If we temporarily don't have internet, don't worry because we'll retry forever with 60 secs sleep in between
    :param url:
    :return:

    """
    returnsoup = ''
    soupmade = False
    while soupmade == False:
        try:
            returnsoup = bs(urllib2.urlopen(url), 'lxml')
            soupmade = True # now we can break out from the while
        except Exception as e:
            print 'exception during opening of site. Trying again in 60 seconds...'
            print 'url was ', url
            time.sleep(60)

    return returnsoup


def tqdm(function, *args, **kwargs):
    """
    we're overriding tqdm to see if tqdm is causing the set changed size during iteration runtimerror
    update: its fixed now, so tqdm sucks I guess

    :param function:
    :return:
    """
    return tqdmlib.tqdm(function, *args, **kwargs)

def random_delay_here():
    if humanize_queries == False:
        return
    delaytime = 3 + random.random() * 4 #avg of 5 sec, between 3 and 7
    time.sleep(delaytime)


def dict_from_gene_url(relativelink):
    """
    Gets the important info from a kegg gene url, returns it as dict
    
    Data we keep for the genes: 
    which enzyme it belongs to 
    kegg entry -> get this from the relativelink
    gene name
    definition
    aa / nucleotide seq
    
    """

    link = 'http://www.genome.jp/dbget-bin/www_bget?' + relativelink
    #print 'dict from gene url link:', link
    random_delay_here()
    oneresult_soup = get_soup_from_url_with_robustness(link)
    res = {}
    try:
        enzymeidinhere = oneresult_soup.find(string=re.compile('KO')).parent.parent.parent.td.getText().strip()
        enzymeid = enzymeidinhere[enzymeidinhere.index('[')+1:enzymeidinhere.index(']')]
    except ValueError:
        enzymeid = None
    #kegg_entry = oneresult_soup.find(string='Entry').parent.parent.parent.td.getText().strip()
    kegg_entry = link[link.index('?')+1:]
    definition = oneresult_soup.find(string='Definition').parent.parent.parent.td.getText() 
        
    aa_string_raw = oneresult_soup.find(string='AA seq').parent.parent.parent.td.getText() 
    aa_string_clean = ''.join(aa_string_raw.splitlines()[1:])
    
    nt_string_raw = oneresult_soup.find(string='NT seq').parent.parent.parent.td.getText() 
    nt_string_clean = ''.join(nt_string_raw.splitlines()[1:])
    
    try:
        gene_name = oneresult_soup.find(string='Gene name').parent.parent.parent.td.getText().strip()
    except:
        gene_name = None
    #genescolumns = ['enzymeid', 'kegg entry', 'gene name', 'definition',
    #               'aa seq', 'nt seq']
    
    res['enzymeid'] = enzymeid #done
    res['kegg entry'] = kegg_entry #done
    res['gene name'] = gene_name #done
    res['definition']= definition #done
    res['aa seq'] = aa_string_clean #done
    res['nt seq'] = nt_string_clean #done
    
    return res

    
def get_gene_links_from_enzymesoup(enzymesoup):
    """
    First click the link 'show more' if there is more
    then get all the hrefs
    make them absolute instead of relative, return them
    """
    
    htmllinks = enzymesoup.find(string='Genes').parent.parent.parent.td.find_all('a')
    links = [link.get("href") for link in htmllinks] #get the actual link
    gene_links = [link for link in links if 'www_bget' in link] #remove the koala links, javascript bs etc
    return gene_links


def get_list_of_pathway_urls(pathways):
    """urls of the pathways always look the same, and so we generate them here."""
    returnlist = []
    for el in pathways:
        return_item = "http://www.kegg.jp/kegg-bin/show_pathway?rn"+str(el)
        returnlist.append(return_item)
    return returnlist
    
    
def get_dict_from_compoundlink(link):
    
    """find keggid, name, reactions"""
    # random_delay_here()
    compoundsoup = get_soup_from_url_with_robustness(link)

    keggid = compoundsoup.find(string='Entry').parent.parent.parent.td.getText()[:6] 
    names = compoundsoup.find(string='Name').parent.parent.parent.td.getText()
    names = ''.join(names)
    reactions = compoundsoup.find(string='Reaction').parent.parent.parent.td.getText() 
    reactions = ' '.join( reactions.split() )
    
    compoundseries = {'keggid': keggid,
                  'names': names,
                  'reactions': reactions}
    return compoundseries
    
        
def get_reaction_db_from_reactions(reactions):
    """
    We know the reaction IDs from the compound DB, now make a DB
    with the reactions (ID, enzyme, input, output)
    """
    print 'Making reaction db from reactions..'
    reactioncolumns = ['keggreactionid', 'enzymeec', 'inputcompounds', 'outputcompounds']
    reactionsdf = pd.DataFrame(columns = reactioncolumns)
    
    #for i, reaction in enumerate(tqdm(reactions)):
    if debug == True:
        reactions = reactions[:10]
    for reaction in tqdm(reactions):
        onereactiondict = get_dict_from_reaction_id(reaction)
        reactionsdf = reactionsdf.append( onereactiondict, ignore_index = True )

        
    reactionsdf.to_csv('reactionsdb.tsv', encoding='utf-8', sep='\t')
   
                      
def get_dict_from_reaction_id(reactionid):
    """
    Return a dictionary of useful stuff from a reactionid, for easy adding
    to the reaction db
    """
    # print 'reactionid is: ', reactionid
    # some reaction IDs are fucked up because kegg can be shit
    # so check if there is an R in it, if not return

    if 'R' not in reactionid:
        returndict = {'keggreactionid': None,
                      'enzymeec': None,
                      'inputcompounds': None,
                      'outputcompounds': None}
        return returndict


    # also accept reactionURLS instead of IDs
    if reactionid.index('R') > 2:
        reactionid = reactionid[reactionid.index('R'):]
    completelink = 'http://www.kegg.jp/dbget-bin/www_bget?'+reactionid
    # random_delay_here()
    reactionsoup = get_soup_from_url_with_robustness(completelink)

    equation = reactionsoup.find(string='Equation').parent.parent.parent.td.getText()
    inputcompounds = equation.split('<=>')[0]
    inputcompounds = inputcompounds.split('+')
    inputcompounds = [compound.strip() for compound in inputcompounds]

    outputcompounds= equation.split('<=>')[1]
    outputcompounds = outputcompounds.split('+')
    outputcompounds = [compound.strip() for compound in outputcompounds]
    try:                   
        enzymeec = reactionsoup.find(string='Enzyme').parent.parent.parent.td.getText().strip()
    except AttributeError:
        enzymeec = None
                       
    returndict = {'keggreactionid': reactionid,
                  'enzymeec': enzymeec,
                  'inputcompounds': ' '.join(inputcompounds),
                  'outputcompounds': ' '.join(outputcompounds)}
    
    return returndict

    
def get_gene_links_from_table(table):
    """
        Gets the endresult of the kegg relational tree: links to invididual gene entries
        We only want results that look like the first one here:
        http://www.kegg.jp/dbget-bin/www_bget?vei:Veis_4270
        http://www.kegg.jp/dbget-bin/www_bget?ko:K07543
    """
      
    all_links = table.find_all('a')
    good_links = []
    
    strictly_links = []
    for link in all_links:
        res = link.get('href')
        strictly_links.append(res)
    
    for link in strictly_links:
        link = str(link)
        
        if ('www_bget?' in link) and ('www_bget?ko' not in link):
            good_links.append(link)
    return good_links
    

def get_compound_db_from_pathway_url(pathwayurl):
    """
    Get all the compound-links, follow those
    get from those: kegg compound ID, name, reactions
    write all this to a CSV
    """
    compoundcolumns = ['keggid', 'names', 'reactions']
    compoundsdf = pd.DataFrame(columns = compoundcolumns)
    
    print 'making compounddb using pathwaysoup of: ', pathwayurl
    if humanize_queries:
        random_delay_here()

    # todo: make an exception handler for internet not working
    pathwaysoup = get_soup_from_url_with_robustness(pathwayurl)
    print 'getting link elements..'
    linkelements = pathwaysoup.find_all('area')
    
    fullcompoundlinks = []
    for link in linkelements:
        relativelink = link.get('href')

        # see if the link is indeed to a compound
        if 'C' in relativelink and 'rn' not in relativelink and 'R' not in relativelink:
            fullink = 'http://www.kegg.jp' + relativelink
            fullcompoundlinks.append(fullink)
    print 'amount of links(among which compounds-link) found in pathway: ', len(fullcompoundlinks)

    if len(fullcompoundlinks) > 10 and debug == True:
        fullcompoundlinks = fullcompoundlinks[:10]
    for i, link in enumerate(tqdm(fullcompoundlinks, smoothing = 0.1)):
        compoundsdf = compoundsdf.append( get_dict_from_compoundlink(link),
                                         ignore_index = True)

    compoundsdf.to_csv('compoundsdb.tsv', encoding='utf-8', sep='\t')


def get_enzyme_db_from_enzyme_ids(ids):
    """
    Get all the enzyme-links, add them to the database
    Columns: EC (=kegg identifier), name, gene ids
    """
    print 'making enzyme db from enzyme ids..'
    enzymecolumns = ['ec', 'name', 'gene ids']
    enzymedf = pd.DataFrame(columns = enzymecolumns)
    
    #first extract the enzyme names from reactions
    
    #look them up, add to the dataframe
    for r in tqdm(ids):
        res = get_dict_from_enzyme_id(r)
        enzymedf = enzymedf.append(res, ignore_index = True)

    time.sleep(0.1) # time for TQDM to finish printing
    print 'enzymesdb made.'
    enzymedf.to_csv('enzymesdb.tsv',encoding='utf-8', sep='\t')


def get_dict_from_enzyme_id(id):
    id = str(id)
    url = 'http://www.kegg.jp/dbget-bin/www_bget?ec:' + id
    random_delay_here()
    enzymesoup = get_soup_from_url_with_robustness(url)

    if 'No such data' in enzymesoup.prettify():
        return None
    
    names = enzymesoup.find(string='Name').parent.parent.parent.td.getText()
    gene_ids = []
    try:
        gene_ids_div = enzymesoup.find(string='Genes').parent.parent.parent.td
        gene_ids_a = gene_ids_div.find_all('a')
        
        for result in gene_ids_a:
            link = result.get('href')
            if 'dbget-bin' not in link:
                break
            gene_id = link[ link.index('?')+1 : ]
            gene_ids.append(gene_id)
        
        
    except AttributeError:
        gene_ids = None
        # print 'This enzyme has no associated genes.'
    
    if (gene_ids != None) and (len(gene_ids) > 1):
        gene_ids = ' '.join(gene_ids)
    
    
    res = { 'ec': id,
            'names': names,
            'gene ids': gene_ids
            }
    return res


def get_gene_db_from_geneids(geneids):
    """
    store the enzyme that we belong to
    kegg entry, gene name, definition, organism
    nucleotide seq, aa seq, 
    
    
    res['enzymeid'] = enzymeid #done
    res['kegg entry'] = kegg_entry #done
    res['gene name'] = gene_name #done
    res['definition']= definition #done
    res['aa seq'] = aa_string_clean #done
    res['nt seq'] = nt_string_clean #done

    Shortcut because otherwise this takes too long: if ngenes > 30, take 30 random genes and download those
    Inspect those to see if we captured enough variance to say this is a representative sample
    """
    print 'making gene db from geneids..'
    
    genedbcolumns = ['kegg entry', 'enzymeid', 'gene name', 'definition', 'aa seq', 'nt seq']
    genedb = pd.DataFrame(columns=genedbcolumns)


    # we have to make the series into a list. Multiple values are in one dataframe entry so we'll have to split that
    geneidlistsplit = []
    for el in geneids:
        if any(ding in str(type(el)) for ding in ['str', 'unicode']):  # if the row is text
            splitgeneids = el.split()
            if splitgeneids.__len__() > 100:
                print str(splitgeneids.__len__()) + ' gene IDS in this cell! Downloading only a random 100.'
                random.shuffle(splitgeneids)
                splitgeneids = splitgeneids[:100]
            geneidlistsplit.extend(splitgeneids)
    geneidlistsplitstripped = [el.strip() for el in geneidlistsplit]

    if debug == True:
        maxgenes = 15
        random.shuffle(geneidlistsplitstripped) # shuffles in place
        geneidlistsplitstripped = geneidlistsplitstripped[:maxgenes]

    for geneid in tqdm(geneidlistsplitstripped):
        res = dict_from_gene_url(geneid)
        genedb = genedb.append(res, ignore_index=True)


    genedb.to_csv('genedb.tsv', encoding='utf-8', sep='\t')


def get_reaction_db_from_pathway(pathwayurl):
    """
       Get all the reaction-links, follow those
       get from those: kegg compound ID, name, reactions
       write all this to a CSV
       """
    reactioncolumns = ['keggreactionid', 'enzymeec', 'inputcompounds', 'outputcompounds']
    reactionsdf = pd.DataFrame(columns = reactioncolumns)

    print 'making reactiondb using pathwaysoup of: ', pathwayurl
    if humanize_queries:
        random_delay_here()

    pathwaysoup = get_soup_from_url_with_robustness(pathwayurl)
    print 'getting link elements..'
    linkelements = pathwaysoup.find_all('area')

    fullreactionlinks = []
    for link in linkelements:
        relativelink = link.get('href')

        # see if the link is indeed to a compound
        # links lead to pages with multiple pages on them, so split them
        splitlink = re.split('[+?]', relativelink)

        for splitel in splitlink:
            if 'R' in splitel and 'RC' not in splitel:
                full_link = 'http://www.kegg.jp/dbget-bin/www_bget?' + splitel
                fullreactionlinks.append(full_link)
    # remove duplicates from the list
    fullreactionlinks = list(set(fullreactionlinks))
    print 'amount of unique  links found in pathway: ', len(fullreactionlinks)

    if len(fullreactionlinks) > 10 and debug == True:
        fullreactionlinks = fullreactionlinks[:10]
    for i, link in enumerate(tqdm(fullreactionlinks, smoothing=0.1)):
        reactionsdf = reactionsdf.append(get_dict_from_reaction_id(link),
                                         ignore_index=True)

        reactionsdf.to_csv('reactionsdb.tsv', encoding='utf-8', sep='\t')

    
    
def start_scraping():
    """
    First get a reactions DB using the pathway
    Then make an enzymes  DB from the reactions database
    Then make a genes DB using the enzymes DB, here get only a subset to avoid downloading too much sequences
    that are extremely similar anyway
    
    Read all the previous data using pandas
    """
    list_of_pathway_urls = get_list_of_pathway_urls(pathways)
    print 'Amount of pathways: ', len(list_of_pathway_urls)
    originaldir = os.getcwd()

    for url in list_of_pathway_urls:
        newdir = originaldir + '/' + url[-5:]
        try:
            os.mkdir(newdir)
        except OSError: #if it already exists, we get this error, but thats fine
            pass
        os.chdir(newdir)
        print 'working dir changed to: ', newdir
        print 'the dir is now: ', os.getcwd()

        #######################################################################
        # Make reaction db from pathway
        if not os.path.isfile('reactionsdb.tsv'):
            get_reaction_db_from_pathway(url)
        else:
            print 'reactionsdb already exists!'
        reactiondb = pd.read_csv('reactionsdb.tsv', index_col=0, encoding='utf-8', sep='\t')

        #######################################################################
        #making of enzyme database
        enzymeseries = reactiondb.enzymeec
        enzymelist = []
        for el in enzymeseries:
            # we cant make an ascii string from latin non-breaking spaces so first unicode normalize it
            if 'float' in str(type(el)): # unless its a float because we can't unicode normalize that...
                el = str(el)
            else:
                unicodeel = el.replace(u'\xa0', u' ')
                el = str(unicodeel)

            splitel = el.split()
            enzymelist.extend(splitel)

        if not os.path.isfile('enzymesdb.tsv'):
            get_enzyme_db_from_enzyme_ids(enzymelist)
        else:
            print 'enzymedb already exists!'
        enzymedb = pd.read_csv('enzymesdb.tsv', index_col = 0, encoding='utf-8', sep='\t')
        
        #######################################################################
        #making of gene database, get gene ids, feed those to the gene db
        geneslist = enzymedb.ix[:,'gene ids']
        if not os.path.isfile('genedb.tsv'):
            get_gene_db_from_geneids(geneslist)
        else:
            print 'Genedb already exists!'
        genedb = pd.read_csv('genedb.tsv', index_col = 0, encoding='utf-8', sep='\t')
        
    print 'Scraping is done! Check the directory of this project for results.'

    
    
if __name__ == '__main__':
    start_scraping()
    
    print 'lets go'
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

def find_links(soup):
    all_links = soup.find_all('a')
    return_list = []
    for link in all_links:
        result = link.get("href") #get is a method on the resultset
        return_list.append(result)
    return return_list
    
        
        
def print_head(page):
    """
    print the first chars of the first lines to get an idea of whats in the page
    """
    for i, line in enumerate(page):
        print i, line[:100]
        if i > 10:
            break
        
def make_list_viewable_variable_explorer(list):
    """
    by default, spyders variable explorer cant see lists returned by beautifulsoup
    so we transform them here
    """
    return_list = []
    for el in list:
        return_list.append(str(el))
    return return_list
        

        
def tuple_from_kegg_name_field(name):
    rows = name.split()
    rows = [ row.replace(';', '' )for row in rows]
    return tuple(rows)